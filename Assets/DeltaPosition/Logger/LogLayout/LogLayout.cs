﻿using NLog.Layouts;

namespace DeltaPosition {
    public abstract class LogLayout {
        public abstract Layout GetLayout();
        public abstract string Build( string message, string stackTrace );
    }
}
