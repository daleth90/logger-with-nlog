﻿using NLog.Layouts;

namespace DeltaPosition {
    public class TextLogLayout : LogLayout {
        private static readonly string LOG_FORMAT = "{0}\n{1}";
        private static readonly Layout LAYOUT = @"${date:format=yyyy-MM-dd HH\:mm\:ss.fff} "
            + "${level:uppercase=true}\n${message}";

        public override Layout GetLayout() {
            return LAYOUT;
        }

        public override string Build( string message, string stackTrace ) {
            return string.Format( LOG_FORMAT, message, stackTrace );
        }
    }
}
