﻿using NLog.Time;
using System;

[TimeSource( "TaiwanTimeSource" )]
public class TaiwanTimeSource : TimeSource {
    private static readonly TimeSpan TIME_SPAN = new TimeSpan( 8, 0, 0 );

    public override DateTime Time {
        get {
            return DateTime.UtcNow.Add( TIME_SPAN ); ;
        }
    }

    public override DateTime FromSystemTime( DateTime systemTime ) {
        return systemTime.Add( TIME_SPAN );
    }
}
