﻿using NLog.Layouts;
using System;
using UnityEngine;

namespace DeltaPosition {
    public class JsonLogLayout : LogLayout {
        [Serializable]
        private class MessageData {
            public int serial = 0;
            public string productName = "";
            public string version = "";
            public string[] message = new string[ 0 ];
            public string[] stackTrace = new string[ 0 ];
        }

        private static readonly object LOCK_OBJECT = new object();
        private static readonly Layout LAYOUT = new JsonLayout() {
            Attributes = {
                new JsonAttribute( "time", @"${date:format=yyyy-MM-dd HH\:mm\:ss.fff}" ),
                new JsonAttribute( "logLevel", "${level:uppercase=true}" ),
                new JsonAttribute( "messageData", "${message}", false )
            },
            MaxRecursionLimit = 2
        };

        private readonly string productName;
        private readonly string version;
        private int serialNumber = 0;

        public JsonLogLayout( string productName, string version ) {
            this.productName = productName;
            this.version = version;
        }

        public override Layout GetLayout() {
            return LAYOUT;
        }

        public override string Build( string message, string stackTrace ) {
            lock ( LOCK_OBJECT ) {
                serialNumber++;
            }

            MessageData data = new MessageData() {
                serial = serialNumber,
                productName = productName,
                version = version,
                message = message.TrimEnd( '\n' ).Split( '\n' ),
                stackTrace = stackTrace.TrimEnd( '\n' ).Split( '\n' )
            };

            return JsonUtility.ToJson( data );
        }
    }
}
