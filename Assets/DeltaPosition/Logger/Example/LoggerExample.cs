﻿using UnityEngine;

namespace DeltaPosition {
    public class LoggerExample : MonoBehaviour {
        private int count;

        private void Start() {
            // Log with all types as normal
            Debug.Log( "debug log message" );
            Debug.LogWarning( "warn log message" );
            Debug.LogAssertion( "assertion log message" );
            Debug.Assert( false, "conditional assertion message" );
            Debug.LogError( "error log message" );

            // Invoke internal error
            Quaternion.LookRotation( new Vector3( 0f, 0f, 0f ) );

            // Log periodically
            InvokeRepeating( "PeriodLog", 1f, 1f );

            // Log Exception
            throw new System.Exception( "exception log message" );
        }

        private void PeriodLog() {
            ++count;
            Debug.LogFormat( "{0} sec", count );
        }
    }
}
