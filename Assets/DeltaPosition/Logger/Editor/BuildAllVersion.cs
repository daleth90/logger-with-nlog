﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace DeltaPosition {
    public class BuildAllVersion {
        private static readonly string[] levels = new string[] { "Assets/DeltaPosition/Logger/Example/Example.unity" };
        private static string buildRootPath;

        private static string GetBuildRootPath() {
            if ( buildRootPath == null ) {
                buildRootPath = new DirectoryInfo( Application.dataPath ).Parent.CreateSubdirectory( "Builds" ).FullName;
            }

            return buildRootPath;
        }

        [MenuItem( "Delta Position/Build/All Backends", false, 0 )]
        private static void BuildAll() {
            BuildMonoDev();
            BuildMono();
            BuildIL2CPPDev();
            BuildIL2CPP();
        }

        [MenuItem( "Delta Position/Build/Mono_Dev", false, 1 )]
        private static void BuildMonoDev() {
            PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x );
            BuildPipeline.BuildPlayer( levels, Path.Combine( GetBuildRootPath(), "Mono_Dev/Example.exe" ), BuildTarget.StandaloneWindows64,
                BuildOptions.Development | BuildOptions.ShowBuiltPlayer );
        }

        [MenuItem( "Delta Position/Build/Mono", false, 2 )]
        private static void BuildMono() {
            PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x );
            BuildPipeline.BuildPlayer( levels, Path.Combine( GetBuildRootPath(), "Mono/Example.exe" ), BuildTarget.StandaloneWindows64,
                BuildOptions.ShowBuiltPlayer );
        }

        [MenuItem( "Delta Position/Build/IL2CPP_Dev", false, 3 )]
        private static void BuildIL2CPPDev() {
            PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP );
            BuildPipeline.BuildPlayer( levels, Path.Combine( GetBuildRootPath(), "IL2CPP_Dev/Example.exe" ), BuildTarget.StandaloneWindows64,
                BuildOptions.Development | BuildOptions.ShowBuiltPlayer );
        }

        [MenuItem( "Delta Position/Build/IL2CPP", false, 4 )]
        private static void BuildIL2CPP() {
            PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP );
            BuildPipeline.BuildPlayer( levels, Path.Combine( GetBuildRootPath(), "IL2CPP/Example.exe" ), BuildTarget.StandaloneWindows64,
                BuildOptions.ShowBuiltPlayer );
        }
    }
}
