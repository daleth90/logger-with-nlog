﻿using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using NLog.Time;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace DeltaPosition {
    public class Logger : MonoBehaviour {
        private class FileLogger {
            public class Setting {
                public int minimumLogLevelOrdinal = 1;
                public string logParentPath = "";
                public bool throwInternalExceptions = false;
                public Layout layout;
            }

            private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
            private readonly string logFolderPath;

            public FileLogger( Setting setting ) {
                LogManager.ThrowExceptions = setting.throwInternalExceptions;
                TimeSource.Current = new TaiwanTimeSource();

                // Step 1. Create configuration object
                LoggingConfiguration config = new LoggingConfiguration();

                // Step 2. Create targets and add them to the configuration
                FileTarget fileTarget = new FileTarget();
                config.AddTarget( "file", fileTarget );

                // Step 3. Set target properties
                logFolderPath = Path.Combine( setting.logParentPath, "Logs" );
                fileTarget.FileName = Path.Combine( logFolderPath, "${date:format=yyyy-MM-dd}/${date:format=yyyy-MM-dd}.txt" );
                fileTarget.Layout = setting.layout;
                fileTarget.KeepFileOpen = true;
                fileTarget.OpenFileCacheTimeout = 30;

                // Step 4. Define rules
                LoggingRule rule = new LoggingRule( "*", LogLevel.FromOrdinal( setting.minimumLogLevelOrdinal ), fileTarget );
                config.LoggingRules.Add( rule );

                // Step 5. Activate the configuration
                LogManager.Configuration = config;
            }

            /// <summary>
            /// This method will delete ANYTHING in the directory, not only logs, older than n days
            /// </summary>
            public void DeleteOldFilesOlderThan( int days ) {
                if ( days <= 0 ) {
                    return;
                }

                try {
                    string[] directories = Directory.GetDirectories( logFolderPath );
                    foreach ( string directory in directories ) {
                        DirectoryInfo directoryInfo = new DirectoryInfo( directory );

                        // Check if the directory is our log directory
                        Match match = Regex.Match( directoryInfo.Name, "[0-9]{4}-[0-9]{2}-[0-9]{2}" );
                        if ( !match.Success ) {
                            continue;
                        }

                        if ( directoryInfo.LastAccessTime < DateTime.Now.AddDays( -days ) ) {
                            directoryInfo.Delete( true );
                        }
                    }
                }
                catch ( Exception e ) {
                    UnityEngine.Debug.LogErrorFormat( "Delete old logs failed!\n{0}", e.Message );
                }
            }

            public void Log( string message, params object[] args ) {
                logger.Debug( message, args );
            }

            public void LogWarning( string message, params object[] args ) {
                logger.Warn( message, args );
            }

            public void LogError( string message, params object[] args ) {
                logger.Error( message, args );
            }

            public void LogException( string message, params object[] args ) {
                logger.Fatal( message, args );
            }
        }

        [Serializable]
        public class Setting {
            public LayoutType layoutType = LayoutType.JSON;
            public string logFolderPath = "";  // Keep empty to set the log folder beside the application
            public int minimumLogLevelOrdinal = 1;  // Trace = 0, Debug = 1, Info = 2, Warn = 3, Error = 4, Fatal = 5, Off = 6
            public int deleteLogsOlderThanDays = 0;  // If the value is less or equal to 0, it won't delete anything
            public bool runInEditor = false;
            public bool dontDestroyOnLoad = true;
            public bool throwInternalExceptions = false;
        }

        public enum LayoutType { TEXT, JSON }

        private FileLogger fileLogger;
        private Setting setting;
        private LogLayout logLayout;

        private void Awake() {
            // Load the setting
            string json = FileUtility.LoadTextFileRelative( "LoggerSetting.json" );
            string cacheError = null;
            if ( string.IsNullOrEmpty( json ) ) {
                cacheError = "LoggerSetting.json is empty. Use default setting instead.";
                setting = new Setting();
            }
            else {
                try {
                    setting = JsonUtility.FromJson<Setting>( json );
                }
                catch ( Exception e ) {
                    cacheError = e.Message;
                    setting = new Setting();
                }
            }

            // Handle DontDestroyOnLoad
            if ( setting.dontDestroyOnLoad ) {
                DontDestroyOnLoad( this );
            }

            if ( setting.runInEditor || !Application.isEditor ) {
                // Create log layout
                if ( setting.layoutType == LayoutType.JSON ) {
                    logLayout = new JsonLogLayout( Application.productName, Application.version );
                }
                else {
                    logLayout = new TextLogLayout();
                }

                // Decide where to put the Log folder
                string logParentPath;
                if ( string.IsNullOrEmpty( setting.logFolderPath ) ) {
                    logParentPath = FileUtility.GetDataPath();
                }
                else {
                    logParentPath = setting.logFolderPath;
                }

                // Create the logger
                FileLogger.Setting loggerSetting = new FileLogger.Setting() {
                    minimumLogLevelOrdinal = setting.minimumLogLevelOrdinal,
                    logParentPath = logParentPath,
                    throwInternalExceptions = setting.throwInternalExceptions,
                    layout = logLayout.GetLayout()
                };

                fileLogger = new FileLogger( loggerSetting );

                // Register the event
                Application.logMessageReceivedThreaded += LogFormat;
            }

            // Log the throwed error while parsing json setting after we register Application.logMessageReceivedThreaded
            if ( !string.IsNullOrEmpty( cacheError ) ) {
                UnityEngine.Debug.LogErrorFormat( "Parse LoggerSetting.json failed! Use default setting instead.\n{0}", cacheError );
            }
        }

        private static string GetDataPath() {
#if !UNITY_EDITOR && ( UNITY_IOS || UNITY_ANDROID )
            return Application.persistentDataPath;
#else
            DirectoryInfo dirInfo = new DirectoryInfo( Application.dataPath );
            return dirInfo.Parent.FullName;
#endif
        }

        /// <summary>
        /// Load text file with relative path
        /// </summary>
        /// <param name="relativePath">File path with the extension, relative to
        /// Application.persistentDataPath in Android/iOS or Application.dataPath.parent in Windows</param>
        private static string LoadTextFileRelative( string relativePath ) {
            string filePath = Path.Combine( GetDataPath(), relativePath );
            using ( FileStream fs = new FileStream( filePath, FileMode.OpenOrCreate ) ) {
                using ( StreamReader reader = new StreamReader( fs ) ) {
                    return reader.ReadToEnd();
                }
            }
        }

        private void Start() {
            if ( setting.runInEditor || !Application.isEditor ) {
                if ( setting.deleteLogsOlderThanDays > 0 ) {
                    fileLogger.DeleteOldFilesOlderThan( setting.deleteLogsOlderThanDays );
                }
            }
        }

        private void OnDestroy() {
            if ( setting.runInEditor || !Application.isEditor ) {
                Application.logMessageReceivedThreaded -= LogFormat;
            }

            // Important: Remember to shutdown NLog
            LogManager.Shutdown();
        }

        private void LogFormat( string condition, string stackTrace, LogType type ) {
            // For LogType.Exception, just log them.
            // Unity keeps them and we can only get them from Unity
            if ( type == LogType.Exception ) {
                fileLogger.LogException( logLayout.Build( condition, stackTrace ) );
            }
            // Else, use StackTrace since Unity don't preserve them on release builds
            else {
                // Create a stack trace which has file infos.
                // We don't skip frames for now in case we lose something important, since the frame we can skip differs by cases
                StackTrace trace = new StackTrace( true );
                if ( type == LogType.Log ) {
                    fileLogger.Log( logLayout.Build( condition, TraceToString( trace ) ) );
                }
                else if ( type == LogType.Warning ) {
                    fileLogger.LogWarning( logLayout.Build( condition, TraceToString( trace ) ) );
                }
                else if ( type == LogType.Assert || type == LogType.Error ) {
                    fileLogger.LogError( logLayout.Build( condition, TraceToString( trace ) ) );
                }
            }
        }

        // Modify the stackTrace to the Unity log style
        private string TraceToString( StackTrace trace ) {
            StringBuilder stringBuilder = new StringBuilder();
            for ( int i = 0; i < trace.FrameCount; i++ ) {
                StackFrame frame = trace.GetFrame( i );

                string fileName = frame.GetFileName();
                int fileLineNumber = frame.GetFileLineNumber();
                MethodBase method = frame.GetMethod();
                string declaringTypeName = method.DeclaringType.FullName;

                if ( fileName == null || fileLineNumber == 0 ) {
                    stringBuilder.AppendFormat( "{0}:{1}({2})", declaringTypeName, method.Name, ParametersToString( method.GetParameters() ) );
                }
                else {
                    stringBuilder.AppendFormat( "{0}:{1}({2}) (at {3}:{4})", declaringTypeName, method.Name, ParametersToString( method.GetParameters() ),
                        fileName.Replace( "\\", "/" ), frame.GetFileLineNumber() );
                }
                stringBuilder.AppendLine();
            }

            string result = stringBuilder.ToString();
            result = result.Replace( "\r\n", "\n" );

            return result;
        }

        private string ParametersToString( ParameterInfo[] parameters ) {
            StringBuilder stringBuilder = new StringBuilder();
            for ( int i = 0; i < parameters.Length; i++ ) {
                if ( i != 0 ) {
                    stringBuilder.Append( ", " );
                }
                stringBuilder.Append( parameters[ i ].ParameterType.Name );
            }

            return stringBuilder.ToString();
        }
    }
}
